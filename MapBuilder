import java.util.ArrayList;
import java.util.Random;

/**
 * MapBuilder generates Random maps for both Single and MultiPlayer Maps, based on the Difficulty and GameType passed in.
 * @author Group 1
 *
 */
public class MapBuilder {
	
	private int height;
	private int width;
	private int maxMoves;
	private int maxNumOfBoxes;
	private int reqNumOfBoxes;
	
	/**
	 * Creates a newMap for a certain Difficulty and GameType
	 * @param currDifficulty
	 * @param currGameType
	 */
	public Map newMap(Difficulty currDifficulty, GameType currGameType) {
		Map currMap = null;
		setMapParameters(currDifficulty);
		//generateSingleMap and generateMultiPlayerMap may return a null if the random Map generated doesn't 
		//produce a certain Number of Boxes. Therefore we keep generating maps until we generate a map
		//that produces the required Number of Boxes
		if (currGameType == GameType.SINGLE) {
			while (currMap == null) {
				currMap = generateSinglePlayerMap(currDifficulty);
			} 
			return currMap;
		} else {
			while (currMap == null) {
				currMap = generateMultiPlayerMap(currDifficulty);
			} 
			return currMap;
		}

	}
	
	/**
	 * Based on the difficulty level, the mapParameters are updated
	 * @param currDifficulty
	 */
	public void setMapParameters(Difficulty currDifficulty) {
		if(currDifficulty == Difficulty.EASY) {
	        height = 7;
	        width = 7;
	        maxMoves = 40;
	        maxNumOfBoxes = 10;
			reqNumOfBoxes = 3;
		} else if (currDifficulty == Difficulty.MEDIUM) {
	        height = 8;
	        width = 8;
	        maxMoves = 60;
	        maxNumOfBoxes = 20;
			reqNumOfBoxes = 4;
		} else if (currDifficulty == Difficulty.HARD) {
	        height = 10;
	        width = 10;
	        maxMoves = 80;
	        maxNumOfBoxes = 30;
			reqNumOfBoxes = 6;
		}
		
	}
	
	/**
	 * generateSinglePlayerMap method will generate a Random Map with a single player on the board. Returns a Map if
	 * the Map generates enough Boxes (i.e. greater than or equal to reqNumOfBoxes). Otherwise returns null.
	 */
	public Map generateSinglePlayerMap(Difficulty currDifficulty) {   
		        
        Random randGen = new Random();
        Map currMap = new Map(height, width);
        //setUpMap will add the walls to the edges of the map and make all the other tiles empty
    	currMap.setUpMap();
    	
    	//initialize starting position
    	int startPlayerRow = randGen.nextInt(height-2) + 1;
    	int startPlayerCol = randGen.nextInt(width-2) + 1;
    	currMap.setPlayer(startPlayerRow, startPlayerCol);
    	MapPosition startingPlayerPosition = new MapPosition(startPlayerRow, startPlayerCol);
    	
    	
    	//Randomly picks the movement number at which the boxes will be placed as the player walks around the Map
    	//(i.e. if 1 is in stepOfPlacement, a box will try to be placed in front of the player when the player makes that many moves)
    	//A box will not always be placed as the player might be in front of a wall or another box
        ArrayList<Integer> stepOfPlacement = new ArrayList<Integer>();
        for(int i = 0; i < maxNumOfBoxes; i++) {
        	int step = randGen.nextInt(maxMoves/2);
        	if(!stepOfPlacement.contains(step)) stepOfPlacement.add(step);
        }
        
        //playerVisited keeps track of the positions where the player has been
        ArrayList<MapPosition> playerVisited = new ArrayList<MapPosition>();
      	//boxLocations keeps track of the positions where we initially placed the boxes
        ArrayList<MapPosition> boxLocations = new ArrayList<MapPosition>();
        playerVisited.add(startingPlayerPosition);
        
        //We add all the possible directions to a direction arrayList
        ArrayList<Direction> direction = new ArrayList<Direction>();
        direction.add(Direction.DOWN);
        direction.add(Direction.UP);
        direction.add(Direction.LEFT);
        direction.add(Direction.RIGHT);
        
        //The player will move randomly through the map until the maxMoves is hit
        for(int i = 0; i < maxMoves; i++) {
        	//we randomly generate the pos (position) of the direction Array that we want to select 
        	int pos = randGen.nextInt(4);
        	//we find the nextPosition if we attempt to move in that direction
        	MapPosition nextPosition = nextMoveSingle(currMap, direction.get(pos));
        	if(currMap.getTileType(nextPosition.getRow(), nextPosition.getColumn()) == TileType.EMPTY && stepOfPlacement.contains(i) 
        			&& !boxLocations.contains(nextPosition) && !playerVisited.contains(nextPosition)) {
            	//if the nextPosition tile is empty and boxLocations doesn't contain the position and playerVisited doesn't contain it as well
            	//and its in stepOfPlacement, we place a Box in the newPosition and add the newPosition to the boxLocations
        		currMap.setBox(nextPosition.getRow(),nextPosition.getColumn());
        		boxLocations.add(nextPosition);
        	}
        	//we then add the newPosition to a playerVisited and move the player
        	playerVisited.add(nextPosition);
        	currMap.movePlayer(direction.get(pos));
        }
        
        //turn current player location to an empty location
        currMap.setEmpty(currMap.getRowPlayer(), currMap.getColPlayer());
        
        //We setGoals where the box's are currently are. We set Walls at all positions the player hasn't visited
        //or box's placed.
		for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				MapPosition currPos = new MapPosition(row,col);
				if(currMap.getTileType(row, col) == TileType.BOX) {
					currMap.setGoal(row, col);
				} else if (!playerVisited.contains(currPos) && !boxLocations.contains(currPos)) {
					currMap.setWall(row, col);
				}
			}
		}
        
		//Place the player in its starting position
		if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.EMPTY) {
			currMap.setPlayer(startPlayerRow, startPlayerCol);
		} else if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.GOAL){
			currMap.setGoalPlayer(startPlayerRow, startPlayerCol);
		}
		
		
		//Place the boxes in their initial positions
		//if you place a box on top of a goal, set it to Empty as this Box has not been moved for Easy and medium difficulty
		//for hard add setGoalBox as it adds an extra level of difficulty
		for(MapPosition mapPos : boxLocations) {
			if(currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.EMPTY) {
				currMap.setBox(mapPos.getRow(), mapPos.getColumn());
			} else if (currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.GOAL) {
				if (currDifficulty == Difficulty.HARD) {
					currMap.setGoalBox(mapPos.getRow(), mapPos.getColumn());
				} else {
					currMap.setEmpty(mapPos.getRow(), mapPos.getColumn());
				}
			}
		}
        
		int numOfNormalBoxes = 0;
        //then we check if there are enough boxes that aren't on goals in the game as some boxes don't move
		for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				if (currMap.getTileType(row, col) == TileType.BOX) numOfNormalBoxes++;
			}
		}
		
		//if there are enough normal Boxes in the Map we return the currMap, otherwise we return null
		if(numOfNormalBoxes >= reqNumOfBoxes) {
			return currMap;
		} else {
			return null;
		}
		
	}
	
	/**
	 * generateMultiPlayerMap method will generate a Random MultiPlayerMap with two players on the board. Returns a MultiPlayerMap if
	 * the MultiPlayerMap has enough Boxes (i.e. greater than or equal to reqNumOfBoxes). Otherwise returns null.
	 */
	public MultiPlayerMap generateMultiPlayerMap(Difficulty currDifficulty) {
		
        Random randGen = new Random();
        MultiPlayerMap currMap = new MultiPlayerMap(height, width);
        //setUpMap will add the walls to the edges of the map and make all the other tiles empty
    	currMap.setUpMap();
		
    	//initialize starting position
    	int startPlayerRow = randGen.nextInt(height-2) + 1;
    	int startPlayerCol = randGen.nextInt(width-2) + 1;
    	currMap.setPlayer(startPlayerRow, startPlayerCol);
    	MapPosition startPlayerPosition = new MapPosition(startPlayerRow, startPlayerCol);
    	MapPosition startPlayerTwoPosition = new MapPosition(startPlayerRow, startPlayerCol);
    	while(startPlayerTwoPosition.equals(startPlayerPosition) ) {
    		startPlayerTwoPosition.setRow(randGen.nextInt(height-2) + 1);
    		startPlayerTwoPosition.setColumn(randGen.nextInt(width-2) + 1);
    	}
		currMap.setPlayerTwo(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn());
    	
    	//Randomly picks the movement number at which the boxes will be placed as the players walks around the Map
    	//(i.e. if 1 is in stepOfPlacement, a box will try to be placed in front of one of the players when the players makes that many moves)
    	//A box will not always be placed as the player might be in front of a wall or another box
        ArrayList<Integer> stepOfPlacement = new ArrayList<Integer>();
        for(int i = 0; i < maxNumOfBoxes; i++) {
        	int step = randGen.nextInt(maxMoves/2);
        	if(!stepOfPlacement.contains(step)) stepOfPlacement.add(step);
        }
        
        //bothOlayerVisited keeps track of the positions where the players has been
        ArrayList<MapPosition> bothPlayerVisited = new ArrayList<MapPosition>();
        
        //boxLocations keeps track of the positions where we initially placed the boxes
        ArrayList<MapPosition> boxLocations = new ArrayList<MapPosition>();
        bothPlayerVisited.add(startPlayerPosition);
        bothPlayerVisited.add(startPlayerTwoPosition);
        
      //We add all the possible directions to a direction arrayList
        ArrayList<Direction> direction = new ArrayList<Direction>();
        direction.add(Direction.DOWN);
        direction.add(Direction.UP);
        direction.add(Direction.LEFT);
        direction.add(Direction.RIGHT);
        
       //The player will move randomly through the map until the maxMoves is hit
       for(int i = 0; i < maxMoves; i++) {
    	    //we randomly generate the pos (position) of the direction Array that we want to select 
        	int pos = randGen.nextInt(4);
        	Boolean movePlayerTwo = false;
        	//we randomly choose which player to move
        	int whoseMove = randGen.nextInt(2);
        	if (whoseMove == 1) {
        		movePlayerTwo = true;
        	}
        	//we find the nextPosition if we attempt to move in that direction
        	MapPosition nextPosition = nextMoveMulti(currMap, direction.get(pos), movePlayerTwo);
        	
        	if(currMap.getTileType(nextPosition.getRow(), nextPosition.getColumn()) == TileType.EMPTY && stepOfPlacement.contains(i) && !boxLocations.contains(nextPosition)
        			&& !bothPlayerVisited.contains(nextPosition)) {
              	//if the nextPosition tile is empty and boxLocations doesn't contain the position and bothPlayerVisited doesn't contain it as well
            	//and its in stepOfPlacement, we place a Box in the newPosition and add the newPosition to the boxLocations
        		currMap.setBox(nextPosition.getRow(),nextPosition.getColumn());
        		boxLocations.add(nextPosition);
        	}
        	
        	//we then add the newPosition to a bothPlayerVisited and move the relevant player
        	bothPlayerVisited.add(nextPosition);
        	if (movePlayerTwo) {
        		currMap.movePlayerTwo(direction.get(pos));
        	} else {
            	currMap.movePlayer(direction.get(pos));
        	}
        }
  
        //turn current player Locations to an empty location
        currMap.setEmpty(currMap.getRowPlayer(), currMap.getColPlayer());
        currMap.setEmpty(currMap.getRowPlayerTwo(), currMap.getColPlayerTwo());
       
        //We setGoals where the box's are currently are. We set Walls at all positions the players hasn't visited
        //or box's placed.
        for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				MapPosition currPos = new MapPosition(row,col);
				if(currMap.getTileType(row, col) == TileType.BOX) {
					currMap.setGoal(row, col);
				} else if (!bothPlayerVisited.contains(currPos) && !boxLocations.contains(currPos)) {
					currMap.setWall(row, col);
				}
			}
		}
        
		//Place player in its starting position
		if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.EMPTY) {
			currMap.setPlayer(startPlayerRow, startPlayerCol);
		} else if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.GOAL){
			currMap.setGoalPlayer(startPlayerRow, startPlayerCol);
		}
        
		//Place playerTwo in its starting position
		if (currMap.getTileType(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn()) == TileType.EMPTY) {
			currMap.setPlayerTwo(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn());	
		} else if (currMap.getTileType(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn()) == TileType.GOAL) {
			currMap.setGoalPlayerTwo(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn());	
		}
		
		
		//Place the boxes in their initial positions
		//if you place a box on top of a goal, set it to Empty as this Box has not been moved for Easy and medium difficulty
		//for hard add setGoalBox as it adds an extra level of difficulty
		for(MapPosition mapPos : boxLocations) {
			if(currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.EMPTY) {
				currMap.setBox(mapPos.getRow(), mapPos.getColumn());
			} else if (currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.GOAL) {
				if (currDifficulty == Difficulty.HARD) {
					currMap.setGoalBox(mapPos.getRow(), mapPos.getColumn());
				} else {
					currMap.setEmpty(mapPos.getRow(), mapPos.getColumn());
				}
			}
		}
        
		int numOfNormalBoxes = 0;
		//then we check if there are enough boxes that aren't on goals in the game as some boxes don't move
		for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				if (currMap.getTileType(row, col) == TileType.BOX) numOfNormalBoxes++;
			}
		}
        
		//if there are enough normal Boxes in the Map we return the currMap, otherwise we return null
		if(numOfNormalBoxes >= reqNumOfBoxes) {
			return currMap;
		} else {
			return null;
		}
    	
	}
	
	/**
	 * nextMoveSingle moves the player in a certain direction. The new position of the player is returned.
	 * @precondition currMap != null, movement != null
	 * @param currMap
	 * @param movement
	 */
	public MapPosition nextMoveSingle(Map currMap, Direction movement) {
		Map temp = currMap.copyMap();
		temp.movePlayer(movement);
    	MapPosition newPosition = new MapPosition(temp.getRowPlayer(), temp.getColPlayer());
    	return newPosition;
	}
	
	/**
	 * nextMoveMulti moves the player or playerTwo in a certain direction. The new position of the player or playerTwo is returned.
	 * If movePlayerTwo is true it will move playerTwo and if movePlayerTwo is false it will move player
	 * @precondition currMap != null, movement != null, movePlayerTwo != null
	 * @param currMap
	 * @param movement
	 * @param movePlayerTwo
	 */
	public MapPosition nextMoveMulti(MultiPlayerMap currMap, Direction movement, Boolean movePlayerTwo) {
		MultiPlayerMap temp = currMap.copyMap();
		MapPosition newPosition;
		if (movePlayerTwo) {
			temp.movePlayerTwo(movement);
	    	newPosition = new MapPosition(temp.getRowPlayerTwo(), temp.getColPlayerTwo());
		} else {
			temp.movePlayer(movement);
	    	newPosition = new MapPosition(temp.getRowPlayer(), temp.getColPlayer());
		}
    	return newPosition;
	}
	
	/**
	 * buildTutorialMap builds and returns a simple tutorial Map
	 */
	public Map buildTutorialMap() {
		Map currMap = new Map(7, 7);
		for(int row = 0; row < 7; row++) {
			for(int col = 0; col < 7; col++) {
				if(row == 0 || col == 0 || row == 6 || col == 6) {
					currMap.setWall(row, col);
				}
				if (col == 2 && row == 1) {
					currMap.setPlayer(row, col);
				}
				if ((col == 3 && row == 2) || (col == 3 && row == 3)) {
					currMap.setBox(row, col);
				}
				if ((col == 5 && row == 5) || (col == 4 && row == 5)) {
					currMap.setGoal(row, col);
				}
			}
		}
		return currMap;
	}
	
	

}
